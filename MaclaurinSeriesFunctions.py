# all functions are here
from math import *


def factorial(x):
    if x == 0:
        return 1
    elif x == 1:
        return 1
    for i in range(1, x):
        x *= i
    return x
	
def log_series(point, presicion):
    if point <= (-1) or point > 1:
        print("(-1,1]")
        exit()
    n = 1
    series = 0
    while (True):
        if abs(point ** n / factorial(n)) < presicion:
            return series
        series += (-1) ** (n + 1) * point ** n / factorial(n)
        n += 1


def alpha_root_series(point, alpha, presicion):
    if abs(point) >= 1:
        print("(-1,1]")
        exit()
    n = 0
    b = 1
    series = 1
    while (True):
        if abs(b * point ** n / factorial(n)) < presicion:
            return series
        if n > 0:
            b *= (alpha - n + 1)
            series += b * point ** n / factorial(n)
        n += 1


def arctg_series(point, presicion):
    if abs(point) > 1:
        print("[-1,1]")
        exit()
    n = 0
    series = 0
    while (True):
        if abs(point ** (2 * n + 1) / (2 * n + 1)) < presicion:
            return series
        series += (-1) ** n * point ** (2 * n + 1) / (2 * n + 1)
        n += 1



def exponent_row(point, precision):
    series = 1
    n = 1
    while(True):
        if abs(point ** n / factorial(n)) < precision:
            return series
        series += point**n / factorial(n)
        n += 1


def sin_series(point, precision):
    point %= 2*pi
    n = 1
    series = 0
    while(True):
        if abs(point ** (2 * n - 1) / factorial(2 * n - 1)) < precision:
            return series
        series += (-1) ** (n + 1) * point ** (2 * n - 1) / factorial(2 * n - 1)
        n += 1


def cos_series(point, precision):
    point %= 2 * pi
    n = 0
    series = 0
    while True:
        if abs(point ** (2 * n) / factorial(2 * n)) < precision:
            return series
        series += (-1) ** n * point ** (2 * n) / factorial(2 * n)
        n += 1

