#test are here
import unittest
import MaclaurinSeriesFunctions
import math

class TestFunction(unittest.TestCase):
    def test_log(self):
        self.assertAlmostEqual(MaclaurinSeriesFunctions.log_series(0.1,0.0001), math.log(1.1), delta=0.0002)
        
        
    def test_alpha_root(self):
        self.assertAlmostEqual(MaclaurinSeriesFunctions.alpha_root_series(0.375,3,0.0001), 1.375 ** 3)
        
        
    def test_arctg(self):
        self.assertAlmostEqual(MaclaurinSeriesFunctions.arctg_series(0.5,0.0000000000000001), math.atan(0.5))
        
        
    def test_exponent(self):
        self.assertAlmostEqual(MaclaurinSeriesFunctions.exponent_row(2, 0.00000001), math.e ** 2)
        self.assertAlmostEqual(MaclaurinSeriesFunctions.exponent_row(3, 0.00000001), math.e ** 3)
        self.assertAlmostEqual(MaclaurinSeriesFunctions.exponent_row(3, 2), math.e ** 3, delta=2.5)
        
        
    def test_sin(self):
        self.assertAlmostEqual(MaclaurinSeriesFunctions.sin_series(10, 0.00000001), math.sin(10))
        self.assertAlmostEqual(MaclaurinSeriesFunctions.sin_series(20, 0.00000001), math.sin(20))
        
        
    def test_cosinus(self):
        self.assertAlmostEqual(MaclaurinSeriesFunctions.cos_series(0, 0.0000001), math.cos(0))
        self.assertAlmostEqual(MaclaurinSeriesFunctions.cos_series(20, 0.0000001), math.cos(20))
        self.assertAlmostEqual(MaclaurinSeriesFunctions.cos_series(math.pi/2, 0.0000001), math.cos(math.pi/2))


if __name__ == '__main__':
    unittest.main()